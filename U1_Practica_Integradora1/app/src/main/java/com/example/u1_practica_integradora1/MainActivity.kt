package com.example.u1_practica_integradora1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var extras = 0
        fun costo(cafe : String): Int {
            var valor = 0
            if (cafe.equals("Expresso (\$30)")){
                valor=30
            }
            if (cafe.equals("Capucchino (\$48)")){
                valor=48
            }
            if (cafe.equals("Americano (\$20)")){
                valor=20
            }
            return valor
        }



        var btnTotal: Button = findViewById(R.id.btnTotal)
        var radioGroup: RadioGroup = findViewById(R.id.radioGroup)
        var editCantidad: EditText=findViewById(R.id.editCantidad)

        var checkAzucar: CheckBox = findViewById(R.id.checkAzucar)
        var checkNomAzucar =""
        checkAzucar.setOnClickListener(View.OnClickListener {
            if (checkAzucar.isChecked) {
                extras++
                checkNomAzucar="Azúcar (\$1) "
            } else {
                extras--
                checkNomAzucar=""
            }
        })
        var checkCrema: CheckBox = findViewById(R.id.checkCrema)
        var checkNomCrema = ""
        checkCrema.setOnClickListener(View.OnClickListener {
            if (checkCrema.isChecked) {
                extras++
                checkNomCrema="Crema (\$1) "
            } else {
                extras--
                checkNomCrema=""
            }
        })
        
        btnTotal.setOnClickListener {
            var cafeGroupId: Int = radioGroup.checkedRadioButtonId
            var radioButonCafe: RadioButton = findViewById(cafeGroupId)

            var cafe: Int = costo(radioButonCafe.text.toString())
            var cantidad: Int = editCantidad.text.toString().toInt()

            var total: Int = (cafe*cantidad)+extras
            var txtResumen: TextView = findViewById(R.id.txtResumen)
            txtResumen.setText(""+cantidad+" "+radioButonCafe.text+" "+checkNomAzucar+checkNomCrema)
            Toast.makeText(this, "Total a pagar: $"+total,Toast.LENGTH_SHORT).show()
        }



    }



}