package com.example.u1_practica9_landscape_portrait

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.ContextCompat

import com.squareup.picasso.Picasso


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show()
        var contador: Int = 1
        val btnNext: Button = findViewById(R.id.btnNext)
        val btnPrevious: Button = findViewById(R.id.btnPrevious)
        var imagen: ImageView = findViewById(R.id.ivImagen)
        var list = listOf("https://s1.eestatic.com/2020/07/29/como/gatos-mascotas-trucos_508959828_156678831_1706x960.jpg",
            "https://www.zooplus.es/magazine/wp-content/uploads/2018/04/fotolia_169457098.jpg",
            "https://www.hogarmania.com/archivos/201204/estrenimiento-gato-bebe2-xl-668x400x80xX.jpg",
            "https://www.batiburrillo.net/wp-content/uploads/2018/09/Cómo-afrontar-la-llegada-de-un-gatito-al-hogar.jpg.webp",
            "https://www.hola.com/imagenes/estar-bien/20180925130054/consejos-para-cuidar-a-un-gatito-recien-nacido-cs/0-601-526/cuidardgatito-t.jpg")
        //https://www.hola.com/imagenes/estar-bien/20180925130054/consejos-para-cuidar-a-un-gatito-recien-nacido-cs/0-601-526/cuidardgatito-t.jpg
        //https://s1.eestatic.com/2020/07/29/como/gatos-mascotas-trucos_508959828_156678831_1706x960.jpg
        //https://www.zooplus.es/magazine/wp-content/uploads/2018/04/fotolia_169457098.jpg
        //https://www.hogarmania.com/archivos/201204/estrenimiento-gato-bebe2-xl-668x400x80xX.jpg
        //https://www.batiburrillo.net/wp-content/uploads/2018/09/Cómo-afrontar-la-llegada-de-un-gatito-al-hogar.jpg.webp

        fun recorrerAdelante(){
            if (contador<4){
                contador++
            }else{
                contador=0
            }
        }
        fun recorrerAtras(){
            if (contador>0){
                contador--
            }else{
                contador=4
            }
        }
        btnNext.setOnClickListener {
            recorrerAdelante()
            //imagen.setImageDrawable(ContextCompat.getDrawable(this, list[contador]))
            Picasso.get().load(list[contador]).into(imagen);
        }
        btnPrevious.setOnClickListener {
            recorrerAtras()
            //imagen.setImageDrawable(ContextCompat.getDrawable(this, list[contador]))
            Picasso.get().load(list[contador]).into(imagen);
        }



    }

    override fun onStart() {
        super.onStart()
        Toast.makeText(this, "onStart", Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        super.onResume()
        Toast.makeText(this, "onResume", Toast.LENGTH_SHORT).show()
    }

    override fun onPause() {
        super.onPause()
        Toast.makeText(this, "onPause", Toast.LENGTH_SHORT).show()
    }

    override fun onStop() {
        super.onStop()
        Toast.makeText(this, "onStop", Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show()
    }

    override fun onRestart() {
        super.onRestart()
        Toast.makeText(this, "onRestart", Toast.LENGTH_SHORT).show()
    }
}
