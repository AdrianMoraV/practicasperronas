package com.example.u1_practica2_layouts

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.Toast


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val btnMostrar: Button =findViewById(R.id.btnMostrar)
        var etName : EditText = findViewById(R.id.edName)
        var etLastName : EditText = findViewById(R.id.edLastName)
        var etAge : EditText = findViewById(R.id.edAge)
        var rbMen : RadioButton = findViewById(R.id.rbMan)
        var rbWoman : RadioButton = findViewById(R.id.rbWoman)

        btnMostrar.setOnClickListener {
            if (etName.text.isNotEmpty() &&etLastName.text.isNotEmpty() && etAge.text.isNotEmpty()){
                 val persona = when {
                    rbMen.isChecked -> {
                        Persona(etName.text.toString(), etLastName.text.toString(), etAge.text.toString().toInt(),"Hombre")
                    }
                    rbWoman.isChecked -> {
                        Persona(etName.text.toString(), etLastName.text.toString(), etAge.text.toString().toInt(),"Mujer")
                    }
                    else -> {
                        Persona(etName.text.toString(), etLastName.text.toString(), etAge.text.toString().toInt(),"Otro")
                    }
                }

                Toast.makeText(this, "Persona de nombre: ${persona.name} ${persona.lastName}\n"+
                "Edad: ${persona.age}\nDe genero: ${persona.gender}", Toast.LENGTH_LONG).show()
            }else{
                Toast.makeText(this, "Porfavor relllena la informacion",Toast.LENGTH_SHORT).show()
            }
        }
    }

}
data class Persona(var name: String, var lastName: String, var age: Int, var gender: String)
