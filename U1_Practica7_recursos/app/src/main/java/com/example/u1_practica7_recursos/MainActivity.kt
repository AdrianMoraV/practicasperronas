package com.example.u1_practica7_recursos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.squareup.picasso.Picasso

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnRecurso : Button = findViewById(R.id.btnRecurso)
        val btnUrl : Button = findViewById(R.id.btnUrl)
        val imagen : ImageView = findViewById(R.id.ivImagen)

        btnRecurso.setOnClickListener{
            imagen.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.imagen2))
        }

        btnUrl.setOnClickListener {
            Picasso.get()
                .load("https://www.hola.com/imagenes/estar-bien/20180925130054/consejos-para-cuidar-a-un-gatito-recien-nacido-cs/0-601-526/cuidardgatito-t.jpg")
                .into(imagen)
        }
    }
}