 package com.example.u1_examen_practico

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*

 class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val boton: Button = findViewById(R.id.btnCalcular)
        var editAño: EditText = findViewById(R.id.editAño)
        var txtExpectativa: TextView = findViewById(R.id.txtExpectativa)
        var txtDeceso: TextView = findViewById(R.id.txtDeceso)
        var groupRegion: RadioGroup = findViewById(R.id.groupRegion)
        var groupGenero: RadioGroup = findViewById(R.id.groupGenero)

        boton.setOnClickListener {
            if(editAño.text.isNotEmpty()){

                val año: Int = editAño.text.toString().toInt()
                val region: RadioButton = findViewById(groupRegion.checkedRadioButtonId)
                val sexo: RadioButton = findViewById(groupGenero.checkedRadioButtonId)
                var difSexo: Boolean =false
                if(sexo.text.toString().equals("Mujer")){
                    difSexo = true
                }

                var expectativa = 0

                if(año in 1950..1959){
                    when(region.text.toString()){
                        "Regiones desarrolladas" ->{
                            expectativa=75
                            if(difSexo){
                                expectativa=expectativa+10
                            }
                        }
                        "América Latina" ->{
                            expectativa=60
                            if(difSexo){
                                expectativa=expectativa+10
                            }
                        }
                        "Asia" ->{
                            expectativa=45
                            if(difSexo){
                                expectativa=expectativa+10
                            }
                        }
                        "Africa" ->{
                            expectativa=35
                            if(difSexo){
                                expectativa=expectativa+10
                            }
                        }
                    }
                }
                if(año in 1960..1969){
                    when(region.text.toString()){
                        "Regiones desarrolladas" ->{
                            expectativa=75
                            if(difSexo){
                                expectativa=expectativa+9
                            }
                        }
                        "América Latina" ->{
                            expectativa=65
                            if(difSexo){
                                expectativa=expectativa+9
                            }
                        }
                        "Asia" ->{
                            expectativa=50
                            if(difSexo){
                                expectativa=expectativa+9
                            }
                        }
                        "Africa" ->{
                            expectativa=45
                            if(difSexo){
                                expectativa=expectativa+9
                            }
                        }
                    }
                }
                if(año in 1970..1979){
                    when(region.text.toString()){
                        "Regiones desarrolladas" ->{
                            expectativa=80
                            if(difSexo){
                                expectativa=expectativa+8
                            }
                        }
                        "América Latina" ->{
                            expectativa=70
                            if(difSexo){
                                expectativa=expectativa+8
                            }
                        }
                        "Asia" ->{
                            expectativa=65
                            if(difSexo){
                                expectativa=expectativa+8
                            }
                        }
                        "Africa" ->{
                            expectativa=55
                            if(difSexo){
                                expectativa=expectativa+8
                            }
                        }
                    }
                }
                if(año in 1980..1989){
                    when(region.text.toString()){
                        "Regiones desarrolladas" ->{
                            expectativa=80
                            if(difSexo){
                                expectativa=expectativa+7
                            }
                        }
                        "América Latina" ->{
                            expectativa=75
                            if(difSexo){
                                expectativa=expectativa+7
                            }
                        }
                        "Asia" ->{
                            expectativa=65
                            if(difSexo){
                                expectativa=expectativa+7
                            }
                        }
                        "Africa" ->{
                            expectativa=60
                            if(difSexo){
                                expectativa=expectativa+7
                            }
                        }
                    }
                }
                if(año in 1990..1999){
                    when(region.text.toString()){
                        "Regiones desarrolladas" ->{
                            expectativa=85
                            if(difSexo){
                                expectativa=expectativa+6
                            }
                        }
                        "América Latina" ->{
                            expectativa=75
                            if(difSexo){
                                expectativa=expectativa+6
                            }
                        }
                        "Asia" ->{
                            expectativa=60
                            if(difSexo){
                                expectativa=expectativa+6
                            }
                        }
                        "Africa" ->{
                            expectativa=55
                            if(difSexo){
                                expectativa=expectativa+6
                            }
                        }
                    }
                }
                if(año >=2000){
                    when(region.text.toString()){
                        "Regiones desarrolladas" ->{
                            expectativa=90
                            if(difSexo){
                                expectativa=expectativa+4
                            }
                        }
                        "América Latina" ->{
                            expectativa=70
                            if(difSexo){
                                expectativa=expectativa+4
                            }
                        }
                        "Asia" ->{
                            expectativa=65
                            if(difSexo){
                                expectativa=expectativa+4
                            }
                        }
                        "Africa" ->{
                            expectativa=60
                            if(difSexo){
                                expectativa=expectativa+4
                            }
                        }
                    }
                }
                txtExpectativa.setText("EXPECTATIVA DE VIDA: "+expectativa)
                txtDeceso.setText("FECHA DE PROBABLE DECESO: "+(año+expectativa))
            }
        }
    }
}