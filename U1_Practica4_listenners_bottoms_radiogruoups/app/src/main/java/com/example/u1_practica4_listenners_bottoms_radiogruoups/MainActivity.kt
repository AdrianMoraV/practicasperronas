package com.example.u1_practica4_listenners_bottoms_radiogruoups

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btnListener: Button =findViewById(R.id.btnListener)
        val btnClase: Button = findViewById(R.id.btnClase)
        btnListener.setOnClickListener {
            Toast.makeText(this, "Boton por listener", Toast.LENGTH_SHORT).show()
        }
        btnClase.setOnClickListener(this)
    }
    fun onXMLClick(view: View){
        Toast.makeText(this, "Boton por XML", Toast.LENGTH_SHORT).show()
    }

    override fun onClick(v: View) {
        Toast.makeText(this, "Boton por Clase", Toast.LENGTH_SHORT).show()
    }
    fun onClick(v: View?){
        val button = v as Button
        when(button.id){
            R.id.btnListener -> {
                Toast.makeText(this, "Btn Listener", Toast.LENGTH_SHORT).show()
            }
            R.id.btnClase -> {
                Toast.makeText(this, "Btn Clase", Toast.LENGTH_SHORT).show()
            }
            R.id.btnXml -> {
                Toast.makeText(this, "Btn XML", Toast.LENGTH_SHORT).show()
            }
        }
    }
}