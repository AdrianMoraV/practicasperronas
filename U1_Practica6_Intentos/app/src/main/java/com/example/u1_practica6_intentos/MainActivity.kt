package com.example.u1_practica6_intentos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import android.content.Intent
import android.util.Pair
import androidx.core.app.ActivityOptionsCompat
import kotlinx.android.synthetic.main.activity_other.*
import android.view.View


const val TEXT_EXTRA = "text_extra"

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        btnGo.setOnClickListener{
            val bundle = Bundle()
           val intent = Intent(this, OtherActivity::class.java)
           // bundle.putString(TEXT_EXTRA, etTexto.text.toString())
           intent.putExtra(TEXT_EXTRA, etTexto.text.toString())
            //val pair:
            //val optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this pair)
           startActivity(intent)


        }
    }



    }
