package com.tec.u1_practica_integradora_2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.u1_practica_integradora2.R
import com.example.u1_practica_integradora2.persona


class CustomAdapter(private val listener: (persona, Int) -> Unit): RecyclerView.Adapter<CustomAdapterViewHolder>() {

    private var list: MutableList<persona> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomAdapterViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.perfil, parent, false)
        return CustomAdapterViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CustomAdapterViewHolder, position: Int) {
        holder.setData(list[position], position, listener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    //PARA AGREGAR VARIOS ITEMS
    fun setList(list: List<persona>){
        this.list.addAll(list)
    }

    //PARA AGREGAR UNA SOLA (NO SE USA EN ESTA PRACTICA)
    fun addPerson(persona: persona){
        this.list.add(persona)
        notifyItemInserted(list.size-1)
    }

}

class CustomAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

    fun setData(persona: persona, position: Int, listener: (persona, Int) -> Unit){
        itemView.apply {
            //ASIGNACION DE VALORES A LA VISTA DE CADA ITEM DE LA LISTA
            var txtNombre: TextView = findViewById(R.id.txtNombre)
            var txtApellido: TextView = findViewById(R.id.txtApellidos)
            var txtEdad: TextView = findViewById(R.id.txtEdad)
            var imagen: ImageView = findViewById(R.id.imageSexo)
            var txtOrg: TextView = findViewById(R.id.txtOrg)
            var txtCorreo: TextView = findViewById(R.id.txtCorreo)
            txtNombre.text = "${persona.nombre}"
            txtApellido.text = "${persona.apellido}"
            txtEdad.text = "(${persona.edad} años)"
            txtOrg.text = "${persona.organizacion}"
            txtCorreo.text = "${persona.correo}"
            if(persona.sexo.equals("Hombre")){
                imagen.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icono_hombre_dos))
            }else{
                imagen.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icono_mujer))
            }

            setOnClickListener {
                listener.invoke(persona, position)
            }
        }
    }

}