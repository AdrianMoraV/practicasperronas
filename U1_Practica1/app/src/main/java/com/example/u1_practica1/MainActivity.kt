package com.example.u1_practica1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var tvText: TextView = findViewById(R.id.tvText)
        tvText.text = "CHA CHA CHA"

        val valor: Int? = null
        val valor2 = 0

        valor?.let { notNullValue ->
            suma(notNullValue, valor2)
        }
        val p1 = Persona("LALO LANDA", 45)
        tvText.text= "Persona1: ${p1.name}, ${p1.age}"

        tvText.setOnClickListener{
            Toast.makeText(this, "HOLA",Toast.LENGTH_SHORT).show()
        }
    }
    fun suma(valor:Int, valor2:Int){

    }
}
data class Persona(var name: String,  var age: Int)